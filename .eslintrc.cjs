module.exports = {
  root: true,
  env: { browser: true, es2020: true },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react-hooks/recommended',
    'airbnb',
    'airbnb-typescript',
  ],
  ignorePatterns: [
    'dist',
    '.eslintrc.cjs',
    'vite.config.ts',
    'tailwind.config.js',
    'postcss.config.js',
    'jest.config.ts',
  ],
  parser: '@typescript-eslint/parser',
  plugins: ['react-refresh'],
  parserOptions: {
    project: ['tsconfig.json']
  },
  rules: {
    'react/jsx-props-no-spreading': 'off',
    'import/no-extraneous-dependencies': 'off',
    'react/require-default-props': 'off',
    'import/prefer-default-export': 'off',
    'react/react-in-jsx-scope': 'off',
    'react/jsx-filename-extension': [2, { 'extensions': ['.ts', '.tsx'] }],
    'react-refresh/only-export-components': [
      'warn',
      { allowConstantExport: true },
    ],
    'linebreak-style': 'off',
    'no-else-return': 'off',
    'react/button-has-type': 'off',
    'react/no-array-index-key': 'off',
    "@typescript-eslint/naming-convention": [
      "error",
      {
        "selector": "enum",
        "format": ["UPPER_CASE"]
      }
    ]
  },
  settings: {
    'import/resolver': {
      typescript: { project: './tsconfig.json' },
      alias: {
        map: [['src', '/src']]
      },
    },
  },
}
