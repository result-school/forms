/** @type {import('tailwindcss').Config} */

import defaultTheme from 'tailwindcss/defaultTheme';

export default {
  content: ['./index.html', './src/**/*.{ts,tsx,css}'],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
      colors: {
        accent: '#E76F51',
        'accent-light': '#FFC595',
        current: '#000000',
        secondary: '#86BBD8',
        theme: '#22796F',
        'theme-dark': '#11574F',
      },
      screens: {
        'xs': '400px',
      }
    },
  },
  plugins: [],
}
