import { defineConfig } from 'vite';
import svgr from "vite-plugin-svgr";
import react from '@vitejs/plugin-react-swc';
import reactRefresh from '@vitejs/plugin-react-refresh';
import { nodeResolve } from '@rollup/plugin-node-resolve';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    reactRefresh(),
    svgr(),
  ],
  resolve: {
    alias: { src: '/src' },
    extensions: ['.ts', '.tsx'],
  },
  build: {
    outDir: 'dist',
    rollupOptions: {
      external: ['lodash-es'],
      plugins: [nodeResolve()],
    },
  },
  server: {
    open: true,
  },
  base: 'https://extraordinary-churros-5a8ef9.netlify.app',
});
