import { Suspense } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { Pages } from 'src/pages';
import { store } from 'src/store';
import './global.css';

export function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Suspense fallback={<p>Loading...</p>}>
          <Pages />
        </Suspense>
      </BrowserRouter>
    </Provider>
  );
}
