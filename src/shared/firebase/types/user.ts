export interface IUser {
  email: string;
  password: string;
  passwordConfirm: string;
}

export interface IProfile {
  email?: string;
  name?: string;
  surname?: string;
  url?: string;
}

export interface RegisterData extends IUser, IProfile {
  email: string;
}

export type LoginData = Omit<IUser, 'passwordConfirm'>;
