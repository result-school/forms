import { Form, Question } from './form';

export interface Answer {
  question: Question;
  answer: string | string[];
}

export interface Response {
  id: string;
  form: Pick<Form, 'id' | 'title' | 'description'>;
  answers: Answer[];
  createdAt: string;
}

export type ResponseFormValues = Pick<Response, 'answers'>;
