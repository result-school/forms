export enum QUESTION_TYPE {
  input = 'input',
  textfield = 'textfield',
  radio = 'radio',
  checkbox = 'checkbox',
}

export interface Question {
  id: string;
  type: QUESTION_TYPE;
  title: string;
  required: boolean;
  answers: string[];
}

export interface Form {
  id: string;
  userId: string;
  title: string;
  description: string;
  questions: Question[];
  /** ISO formatted date */
  createdAt: string;
  responseCount?: number;
}

export type FormValues =
  Pick<Form, 'title' | 'description' | 'questions'>;
