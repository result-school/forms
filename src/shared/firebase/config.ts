import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';

const firebase = {
  apiKey: 'AIzaSyBITkZaKQv1mQNGJZ4v0NyDKrb9esqSfyg',
  authDomain: 'groupprojectforms.firebaseapp.com',
  projectId: 'groupprojectforms',
  storageBucket: 'groupprojectforms.appspot.com',
  messagingSenderId: '779774743331',
  appId: '1:779774743331:web:55442b8475110a6cc1f296',
};

export const app = initializeApp(firebase);
export const auth = getAuth(app);
export const db = getFirestore(app);

export enum COLLECTIONS {
  forms = 'forms',
  responses = 'responses',
  users = 'users',
}
