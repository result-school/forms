import { FieldProps } from 'formik/dist/Field';
import './styles.scss';

interface ChoiceInputProps extends FieldProps {
  id: string;
  value: string;
  type: 'checkbox' | 'radio';
  required?: boolean;
}

export function ChoiceInput({
  id,
  value,
  type,
  required,
  field,
}: ChoiceInputProps) {
  return (
    <label htmlFor={id} className="choice-label">
      <input
        id={id}
        {...field}
        type={type}
        value={value}
        required={required}
        className="choice-input"
      />
      <span className="choice-value">{value}</span>
    </label>
  );
}
