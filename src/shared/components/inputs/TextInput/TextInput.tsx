import classNames from 'classnames';
import { ChangeEvent } from 'react';
import { FieldProps } from 'formik/dist/Field';
import './styles.scss';

interface InputProps extends Partial<FieldProps> {
  required?: boolean;
  name: string;
  type: string;
  placeholder?: string;
  disabled?: boolean;
  error?: string;
  value?: string;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  onBlur?: () => void;
  light?: boolean;
  center?: boolean;
  label?: string;
  className?: string;
}

export function TextInput({
  required = false,
  type = 'text',
  placeholder = '',
  disabled = false,
  error = '',
  className,
  light,
  center,
  form,
  field,
  meta,
  label,
  ...props
}: InputProps) {
  return (
    <div className="form-control">
      {label?.length && <p>{label}</p>}
      <input
        type={type}
        placeholder={placeholder}
        required={required}
        disabled={disabled}
        {...field}
        {...props}
        className={classNames('input', {
          center, light, disabled, invalid: error,
        }, className)}
      />
      <span className="error-text">
        {error || meta?.error}
      </span>
    </div>
  );
}
