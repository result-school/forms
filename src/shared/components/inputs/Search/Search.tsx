import { ChangeEvent } from 'react';
import './style.scss';

interface InterfaceSearch {
  handlerSearch: (value: string) => void;
}

export function Search({ handlerSearch }: InterfaceSearch) {
  const onSearch = (event: ChangeEvent<HTMLInputElement>): void => {
    handlerSearch(event.target.value);
  };

  return (
    <div className="search">
      <input className="search-input" type="search" placeholder="Поиск..." onChange={onSearch} />
    </div>
  );
}
