import { ChevronDownIcon } from '@heroicons/react/24/outline';
import './styles.scss';
import { ChangeEvent } from 'react';
import { FieldProps } from 'formik/dist/Field';

interface SelectProps extends Partial<FieldProps> {
  onChange: (e: ChangeEvent<HTMLSelectElement>) => void;
  value?: string;
  options: {
    [key: string]: string;
  }
}

export function Select({
  options,
  form,
  field,
  meta,
  ...props
}: SelectProps) {
  return (
    <div className="select-wrapper">
      <ChevronDownIcon width={16} className="select-icon" color="black" />
      <select
        title="Выбор варианта"
        {...field}
        {...props}
        className="select"
      >
        {Object.keys(options).map((key) => (
          <option key={key} value={key}>
            {options[key]}
          </option>
        ))}
      </select>
    </div>
  );
}
