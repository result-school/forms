import { ChangeEvent } from 'react';
import './styles.scss';

interface DateInputProps {
  name: string;
  value: string;
  placeholder: string;
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

export function DateInput({
  name,
  value,
  placeholder,
  onChange,
}: DateInputProps) {
  return (
    <input
      type="date"
      name={name}
      value={value}
      placeholder={placeholder}
      onChange={onChange}
      className="date-input"
    />
  );
}
