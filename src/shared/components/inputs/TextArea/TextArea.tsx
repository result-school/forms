import classNames from 'classnames';
import { FieldProps } from 'formik/dist/Field';
import './styles.scss';

interface TextAreaProps extends FieldProps {
  disabled?: boolean;
  required?: boolean;
  error?: string;
}

export function TextArea({
  disabled,
  required,
  error,
  field,
  meta,
}: TextAreaProps) {
  return (
    <div className="form-control">
      <textarea
        {...field}
        rows={5}
        wrap="hard"
        required={required}
        className={classNames('textarea', {
          disabled, error,
        })}
      />
      <span className="error-text">
        {error || meta?.error}
      </span>
    </div>
  );
}
