import classNames from 'classnames';
import { ChangeEvent } from 'react';
import { FieldProps } from 'formik/dist/Field';
import { EyeSlashIcon, EyeIcon } from '@heroicons/react/24/outline';
import './styles.scss';

interface InputProps extends Partial<FieldProps> {
  required?: boolean;
  name: string;
  type: string;
  placeholder?: string;
  disabled?: boolean;
  error?: string;
  value?: string;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  onBlur?: () => void;
  light?: boolean;
  center?: boolean;
  label?: string;
  show: boolean;
  onClick: (e: React.MouseEvent<HTMLButtonElement>, arg: boolean) => void;
}

export function PasswordInput({
  required = false,
  type,
  placeholder = '',
  disabled = false,
  error = '',
  light,
  center,
  form,
  field,
  meta,
  label,
  show,
  onClick,
  ...props
}: InputProps) {
  return (
    <div className="form-control">
      {label?.length && <p>{label}</p>}
      <div className="relative">
        <input
          type={type}
          placeholder={placeholder}
          required={required}
          disabled={disabled}
          {...field}
          {...props}
          className={classNames('input', {
            center, light, disabled, invalid: error,
          })}
        />
        <button
          className="btn-eye"
          onClick={(e) => onClick(e, show)}
        >
          {show ? <EyeSlashIcon /> : <EyeIcon />}
        </button>
      </div>

      <span className="error-text">
        {error || meta?.error}
      </span>
    </div>
  );
}
