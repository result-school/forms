import type { PropsWithChildren } from 'react';
import './styles.scss';
import { FieldProps } from 'formik/dist/Field';

interface ToggleProps extends FieldProps {
  toggled?: boolean;
  name: string;
}

export function Toggle({
  children, name,
  form,
  field,
  ...props
}: PropsWithChildren<ToggleProps>) {
  return (
    <label className="toggle" htmlFor={name}>
      {children}
      <input
        type="checkbox"
        id={name}
        {...field}
        {...props}
        className="toggle-input"
      />
      <span className="toggle-switch" hidden />
    </label>
  );
}
