import { PropsWithChildren } from 'react';
import './styles.scss';

interface DescriptionProps {}

export function Description({ children }: PropsWithChildren<DescriptionProps>) {
  return (
    <p className="page-description">
      {children}
    </p>
  );
}
