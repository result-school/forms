import './styles.scss';

export function Loader() {
  return (
    <div className="loader__wrapper">
      <div className="loader" />
    </div>
  );
}
