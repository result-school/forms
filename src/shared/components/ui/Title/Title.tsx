import { PropsWithChildren } from 'react';
import './styles.scss';

interface TitleProps {}

export function Title({ children }: PropsWithChildren<TitleProps>) {
  return (
    <h1 className="page-title">
      {children}
    </h1>
  );
}
