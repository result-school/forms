import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { CheckIcon, Square2StackIcon, XMarkIcon } from '@heroicons/react/24/outline';
import { Button } from '../Button';
import './styles.scss';

interface ModalProps {
  title: string;
  description?: string;
  link?: string;
  open: boolean;
  onClose?: () => void;
  withReturnButton?: boolean;
}

export function Modal({
  title,
  description,
  link,
  open,
  onClose,
  withReturnButton,
}: ModalProps) {
  const navigate = useNavigate();
  const [copied, setCopied] = useState(false);

  const handleBack = () => {
    navigate('/');
  };

  const handleCopy = () => {
    if (link) navigator.clipboard.writeText(link);
    setCopied(true);
  };

  if (!open) return null;

  return (
    <div className="modal-wrapper">
      <div className="modal">
        {onClose && (
          <div className="modal-close">
            <Button light iconOnly onClick={onClose}>
              <XMarkIcon width={16} />
              <span className="sr-only">Закрыть</span>
            </Button>
          </div>
        )}
        <h2 className="modal-title">{title}</h2>
        {description && (
          <p className="modal-description">{description}</p>
        )}
        {link && (
          <button className="link-to-copy" onClick={handleCopy}>
            <p className="link-content">{link}</p>
            <span className="link-icon">
              {copied ? (
                <CheckIcon width={16} />
              ) : (
                <Square2StackIcon width={16} />
              )}
            </span>
            <span className="sr-only">Скопировать ссылку</span>
          </button>
        )}
        {withReturnButton && (
          <div className="modal-buttons">
            <Button onClick={handleBack}>На главную</Button>
          </div>
        )}
      </div>
    </div>
  );
}
