import { PropsWithChildren } from 'react';
import './styles.scss';

interface TileProps {
  className?: string;
}

export function Tile({ children, className }: PropsWithChildren<TileProps>) {
  return (
    <div className={`tile ${className ?? ''}`}>
      {children}
    </div>
  );
}
