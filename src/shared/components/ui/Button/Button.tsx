import classNames from 'classnames';
import { ButtonHTMLAttributes, PropsWithChildren } from 'react';
import './styles.scss';

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  fullWidth?: boolean;
  onClick?: () => void;
  disabled?: boolean;
  light?: boolean;
  iconOnly?: boolean;
}

export function Button({
  children, fullWidth, onClick, disabled, light, iconOnly,
  type = 'button',
  ...props
}: PropsWithChildren<ButtonProps>) {
  return (
    <button
      onClick={onClick}
      className={classNames('btn', {
        light,
        iconOnly,
        disabled,
        'w-full': fullWidth,
      })}
      disabled={disabled}
      type={type}
      {...props}
    >
      {children}
    </button>
  );
}
