import { Field } from 'formik';
import { QUESTION_TYPE, Question } from 'src/shared/firebase/types';
import { ChoiceInput, TextArea, TextInput } from '../../inputs';
import './styles.scss';

interface AnswerCardProps {
  index: number;
  question: Question;
}

export function AnswerCard({
  index,
  question,
}: AnswerCardProps) {
  const hasMultipleAnswers = [
    QUESTION_TYPE.radio,
    QUESTION_TYPE.checkbox,
  ].includes(question.type);

  return (
    <div className="answer-card">
      <p className="question-title">
        {question.title}
        {question.required && (
          <span className="required">*</span>
        )}
      </p>
      {hasMultipleAnswers && (
        <fieldset className="answer-options">
          {question.answers.map((answer, answerIndex) => (
            <Field
              key={answerIndex}
              id={`answer.${index}.${answerIndex}`}
              name={`answers.${index}.answer`}
              value={answer}
              type={question.type}
              component={ChoiceInput}
            />
          ))}
        </fieldset>
      )}
      {question.type === QUESTION_TYPE.input && (
        <Field
          name={`answers.${index}.answer`}
          component={TextInput}
          required={question.required}
        />
      )}
      {question.type === QUESTION_TYPE.textfield && (
        <Field
          name={`answers.${index}.answer`}
          component={TextArea}
          required={question.required}
        />
      )}
    </div>
  );
}
