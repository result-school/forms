import { useState } from 'react';
import { useDrag, useDrop } from 'react-dnd';
import { TrashIcon } from '@heroicons/react/24/outline';
import { Field, useFormikContext } from 'formik';
import { FormValues, QUESTION_TYPE, Question } from 'src/shared/firebase/types';
import { Select, TextInput, Toggle } from '../../inputs';
import { Button } from '../../ui';
import { typeTextMap } from './constants';
import { AnswerList } from './AnswerList';
import './styles.scss';

interface QuestionCardProps {
  id: string;
  question: Question;
  index: number,
  coordData: DOMRect,
  onDelete: (id: string) => void;
  moveCard: (id: string, atIndex: number) => void;
  findQuestion: (id: string) => { question: Question, index: number };
}

interface IDraggableItem {
  id: string;
  originalIndex: number;
}

export function QuestionCard({
  id,
  index,
  question,
  coordData,
  onDelete,
  moveCard,
  findQuestion,
}: QuestionCardProps) {
  const { values, errors } = useFormikContext<FormValues>();
  const [shadow, setShadow] = useState<boolean>(false);
  const { answers } = values.questions[index];
  const baseName = `questions.${index}`;

  const hasMultipleAnswers = [
    QUESTION_TYPE.radio,
    QUESTION_TYPE.checkbox,
  ].includes(question.type);

  const originalIndex = findQuestion(id).index;
  const parentLeft: number = +coordData.left.toFixed();
  const parentRight: number = +coordData.right.toFixed();
  const parentTop: number = +coordData.top.toFixed();
  const parentBottom: number = +coordData.bottom.toFixed();

  const [, drag] = useDrag(() => ({
    type: 'input',
    item: { id, originalIndex },
    collect: (monitor) => ({
      isDrag: monitor.isDragging(),
    }),
    end: (item, monitor) => {
      const dropResult = monitor.getDropResult();
      if (dropResult) {
        moveCard(item.id, originalIndex);
      }
      const clientOffset = monitor.getClientOffset();
      const clientOffsetX = clientOffset ? +clientOffset.x.toFixed() : 0;
      const clientOffsetY = clientOffset ? +clientOffset.y.toFixed() : 0;

      if (clientOffsetX && clientOffsetY) {
        if ((clientOffsetX < parentLeft || clientOffsetX > parentRight)
          && (clientOffsetY > parentBottom || clientOffsetY < parentTop)) {
          onDelete(item.id);
        } else if (clientOffsetX > parentRight && clientOffsetY < parentBottom
          && clientOffsetY > parentTop) {
          onDelete(item.id);
        } else if (clientOffsetX < parentLeft && clientOffsetY < parentBottom
          && clientOffsetY > parentTop) {
          onDelete(item.id);
        } else if (clientOffsetY > parentBottom && clientOffsetX > parentLeft
          && clientOffsetX < parentRight) {
          onDelete(item.id);
        } else if (clientOffsetY < parentTop && clientOffsetX > parentLeft
          && clientOffsetX < parentRight) {
          onDelete(item.id);
        } else {
          moveCard(item.id, originalIndex);
        }
      }
    },
    isDragging(monitor) {
      setShadow(false);
      const offset = monitor.getClientOffset();
      const clientOffsetX = offset ? +offset.x.toFixed() : 0;
      const clientOffsetY = offset ? +offset.y.toFixed() : 0;
      if (clientOffsetX && clientOffsetY) {
        if ((clientOffsetX < parentLeft || clientOffsetX > parentRight)
          && (clientOffsetY > parentBottom || clientOffsetY < parentTop)) {
          setShadow(true);
        } else if (clientOffsetX > parentRight && clientOffsetY < parentBottom
          && clientOffsetY > parentTop) {
          setShadow(true);
        } else if (clientOffsetX < parentLeft && clientOffsetY < parentBottom
          && clientOffsetY > parentTop) {
          setShadow(true);
        } else if (clientOffsetY > parentBottom && clientOffsetX > parentLeft
          && clientOffsetX < parentRight) {
          setShadow(true);
        } else if (clientOffsetY < parentTop && clientOffsetX > parentLeft
          && clientOffsetX < parentRight) {
          setShadow(true);
        } else {
          setShadow(false);
        }
      }
      return shadow;
    },
  }), [id, originalIndex, moveCard]);

  const [, drop] = useDrop(() => ({
    accept: 'input',
    collect: (monitor) => ({
      isOver: monitor.isOver(),
    }),
    hover: (item: IDraggableItem) => {
      const dragIndex = item.originalIndex;
      const hoverIndex = index;
      if (dragIndex === hoverIndex) return;
      moveCard(item.id, hoverIndex);
    },
  }), [moveCard]);

  const errorMessage = Array.isArray(errors.questions) && errors.questions[index];

  return (
    <div
      className={shadow ? 'question-card question-card--delete' : 'question-card'}
      ref={(node) => drag(drop(node))}
    >
      <div className="body">
        <div className="inputs">
          <Field
            name={`${baseName}.title`}
            placeholder="Введите вопрос"
            component={TextInput}
            error={
              !!errorMessage
              && typeof errorMessage === 'object'
              && errorMessage.title
            }
          />
          {hasMultipleAnswers && (
            <Field
              name={`${baseName}.type`}
              component={Select}
              options={{
                radio: typeTextMap.radio,
                checkbox: typeTextMap.checkbox,
              }}
            />
          )}
        </div>
        {hasMultipleAnswers ? (
          <AnswerList
            answers={answers}
            baseName={baseName}
            questionType={question.type}
            errors={
              !!errorMessage
              && typeof errorMessage === 'object'
              && errorMessage.answers
            }
          />
        ) : (
          <p className="answer">
            {typeTextMap[question.type]}
          </p>
        )}
      </div>
      <div className="actions">
        <Field name={`questions.${index}.required`} component={Toggle}>
          Обязательный вопрос
        </Field>
        <div className="delete">
          <Button light iconOnly onClick={() => onDelete(id)}>
            <TrashIcon color="black" width={16} />
            <span className="sr-only">Удалить</span>
          </Button>
        </div>
      </div>
    </div>
  );
}
