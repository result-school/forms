import { QUESTION_TYPE } from 'src/shared/firebase/types';

export const typeTextMap: Record<QUESTION_TYPE, string> = {
  input: 'Короткий текст',
  textfield: 'Длинный текст',
  radio: 'Один из списка',
  checkbox: 'Несколько из списка',
};
