import { PlusIcon, XMarkIcon } from '@heroicons/react/24/outline';
import classNames from 'classnames';
import { Field, FieldArray } from 'formik';
import { KeyboardEvent } from 'react';
import { QUESTION_TYPE } from 'src/shared/firebase/types';
import { TextInput } from '../../inputs';
import { Button } from '../../ui';

interface AnswerListProps {
  answers: string[];
  baseName: string;
  questionType: QUESTION_TYPE;
  errors?: string | string[] | false;
}

export function AnswerList({
  answers, baseName, questionType, errors,
}: AnswerListProps) {
  return (
    <FieldArray
      name={`${baseName}.answers`}
      render={(arrayHelpers) => (
        <div className="pt-2 pb-4">
          <div className="answers">
            {answers.map((_, indexAnswer) => (
              <div key={indexAnswer} className="answer-item">
                <span className={classNames('answer-icon', questionType)} />
                <Field
                  name={`${baseName}.answers.${indexAnswer}`}
                  component={TextInput}
                  placeholder="Вариант ответа"
                  onKeyDown={(event: KeyboardEvent<HTMLInputElement>) => {
                    if (event.key === 'Enter') {
                      event.preventDefault();
                      arrayHelpers.push('');
                    }
                  }}
                  autoFocus
                  light
                  error={
                    Array.isArray(errors)
                    && errors[indexAnswer]
                  }
                />
                {answers.length > 1 && (
                  <Button
                    light
                    iconOnly
                    onClick={() => arrayHelpers.remove(indexAnswer)}
                  >
                    <XMarkIcon color="black" width={16} />
                    <span className="sr-only">Удалить вариант</span>
                  </Button>
                )}
              </div>
            ))}
            {typeof errors === 'string' && (
              <p>{errors}</p>
            )}
          </div>
          <Button light onClick={() => arrayHelpers.push('')}>
            <PlusIcon width={16} />
            <span className="pl-3">Добавить вариант</span>
          </Button>
        </div>
      )}
    />
  );
}
