import { ReactNode } from 'react';
import { useDrag } from 'react-dnd';
import { Question } from 'src/shared/firebase/types';
import { ConstructorItemType } from './types';
import './styles.scss';

interface IConstructorItem {
  id: string,
  label: string;
  icon: ReactNode;
  type: string;
}

export function OptionCard({
  id, icon, label, type,
}: IConstructorItem) {
  const [{ isDragging }, dragRef] = useDrag(() => ({
    type: ConstructorItemType.FIELD,
    item: {
      id,
      title: '',
      type,
      required: false,
      answers: [''],
    } as Question,
    collect: (monitor) => ({
      isDragging: !!monitor.isDragging(),
    }),
  }));

  return (
    <div
      ref={dragRef}
      id={id}
      className="option-card"
      style={{ opacity: isDragging ? '0.2' : '1' }}
    >
      {icon}
      <span className="option-card__label">
        {label}
      </span>
    </div>
  );
}
