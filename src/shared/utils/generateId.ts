export function generateId(max: number): string {
  return Math.floor(Math.random() * max).toString();
}
