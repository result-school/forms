import dayjs from 'dayjs';

interface SortableItem {
  /** ISO formatted date */
  createdAt: string;
}

export function sortByDate(arr: SortableItem[], type: string) {
  return arr.sort((a, b) => {
    switch (type) {
      case 'ascDate':
        return dayjs(a.createdAt).isAfter(dayjs(b.createdAt), 'date') ? 1 : -1;
      case 'descDate':
        return dayjs(a.createdAt).isBefore(dayjs(b.createdAt), 'date') ? 1 : -1;
      case 'ascTime':
        return dayjs(a.createdAt).isAfter(dayjs(b.createdAt)) ? 1 : -1;
      case 'descTime':
        return dayjs(a.createdAt).isBefore(dayjs(b.createdAt)) ? 1 : -1;
      default:
        return 0;
    }
  });
}
