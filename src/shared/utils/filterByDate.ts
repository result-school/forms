import dayjs from 'dayjs';

interface FilterableItem {
  /** ISO formatted date */
  createdAt: string;
}

interface FilterParams {
  startDate: string;
  endDate: string;
}

export function filterByDate(arr: FilterableItem[], params: FilterParams) {
  return arr.filter((item) => {
    const createdAfterStart = params.startDate
      ? !dayjs(item.createdAt).isBefore(dayjs(params.startDate), 'date')
      : true;

    const createdBeforeEnd = params.endDate
      ? !dayjs(item.createdAt).isAfter(dayjs(params.endDate), 'date')
      : true;

    return createdAfterStart && createdBeforeEnd ? item : false;
  });
}
