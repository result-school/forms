import { Suspense } from 'react';
import { Outlet } from 'react-router';
import './styles.css';

export function AuthLayout() {
  return (
    <div className="auth-container">
      <Suspense>
        <Outlet />
      </Suspense>
    </div>
  );
}
