import { Fragment, useEffect, useState } from 'react';
import { usersSlice } from 'src/store/slices';
import { IProfile } from 'src/shared/firebase/types';
import { Link, NavLink } from 'react-router-dom';
import { Disclosure, Menu, Transition } from '@headlessui/react';
import { Bars3Icon, XMarkIcon } from '@heroicons/react/24/outline';
import { UserCircleIcon } from '@heroicons/react/16/solid';
import { navigation, userNavigation } from './constants';
import './styles.css';

interface HeaderProps {
  onLogout: () => void;
}

export function Header({ onLogout }: HeaderProps) {
  const [user, setUser] = useState<IProfile | undefined>();
  const { data } = usersSlice.useGetProfileQuery();

  useEffect(() => {
    setUser(data as IProfile);
  }, [data]);

  return (
    <Disclosure as="nav" className="header">
      {({ open, close }) => (
        <>
          <div className="header-grid">
            <Disclosure.Button className="mobile-menu-button">
              <span className="sr-only">Open main menu</span>
              {open ? (
                <XMarkIcon className="mobile-menu-icon" aria-hidden="true" />
              ) : (
                <Bars3Icon className="mobile-menu-icon" aria-hidden="true" />
              )}
            </Disclosure.Button>
            <div className="navigation">
              {navigation.map((item) => (
                <NavLink
                  key={item.name}
                  to={item.path}
                  className={({ isActive }) => `navigation-item ${isActive && 'active'}`}
                >
                  {item.name}
                </NavLink>
              ))}
            </div>
            <div className="user-menu-wrapper">
              <Menu
                as="div"
                className="user-menu"
              >
                <Menu.Button className="user-menu-button">
                  <span className="sr-only">Open user menu</span>
                  { user && user.url
                    ? (<img src={user.url} alt="avatar" className="header-avatar" />)
                    : (<UserCircleIcon width={36} className="user-img" />)}
                </Menu.Button>
                <Transition
                  as={Fragment}
                  enter="transition ease-out duration-100"
                  enterFrom="transform opacity-0 scale-95"
                  enterTo="transform opacity-100 scale-100"
                  leave="transition ease-in duration-75"
                  leaveFrom="transform opacity-100 scale-100"
                  leaveTo="transform opacity-0 scale-95"
                >
                  <Menu.Items className="user-navigation">
                    {userNavigation.map((item) => (
                      <Menu.Item key={item.name}>
                        <Link to={item.path} className="user-navigation-item">
                          {item.name}
                        </Link>
                      </Menu.Item>
                    ))}
                    <Menu.Item>
                      <button
                        className="user-navigation-item w-full text-start"
                        type="button"
                        onClick={onLogout}
                      >
                        Выход
                      </button>
                    </Menu.Item>
                  </Menu.Items>
                </Transition>
              </Menu>
            </div>
          </div>
          <Disclosure.Panel className="sm:hidden">
            <div className="mobile-menu">
              {navigation.map((item) => (
                <NavLink
                  key={item.name}
                  to={item.path}
                  className={({ isActive }) => `mobile-menu-item ${isActive && 'active'}`}
                  onClick={() => close()}
                >
                  {item.name}
                </NavLink>
              ))}
            </div>
          </Disclosure.Panel>
        </>
      )}
    </Disclosure>
  );
}
