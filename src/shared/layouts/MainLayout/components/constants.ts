export const navigation = [
  { name: 'Главная', path: '/' },
  { name: 'Новая форма', path: '/forms/new' },
];

export const userNavigation = [
  { name: 'Профиль', path: '/me' },
];
