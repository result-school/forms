import { Suspense } from 'react';
import { Outlet, useNavigate } from 'react-router';
import { signOut } from 'firebase/auth';
import { auth } from 'src/shared/firebase';
import { Header } from './components/Header';
import './styles.css';

export function MainLayout() {
  const navigate = useNavigate();

  const handleLogout = () => {
    signOut(auth).then(() => {
      navigate('/login');
    }).catch((error) => {
      console.log(error);
    });
  };

  return (
    <div className="page-wrapper">
      <Header onLogout={handleLogout} />
      <div className="content-wrapper">
        <Suspense>
          <Outlet />
        </Suspense>
      </div>
    </div>
  );
}
