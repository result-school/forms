import { Outlet } from 'react-router';
import { Suspense } from 'react';

export function PublicRoute() {
  return (
    <Suspense>
      <Outlet />
    </Suspense>
  );
}
