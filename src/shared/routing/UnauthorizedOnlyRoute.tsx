import { Suspense, useEffect } from 'react';
import { Outlet, useNavigate, useLocation } from 'react-router';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { app } from 'src/shared/firebase';

export function UnauthorizedOnlyRoute() {
  const navigate = useNavigate();
  const auth = getAuth(app);
  const location = useLocation();

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user?.uid) {
        navigate('/', { state: { from: location } });
      } else {
        navigate('/login');
      }
    });
  }, [auth]);

  return (
    <Suspense>
      <Outlet />
    </Suspense>
  );
}
