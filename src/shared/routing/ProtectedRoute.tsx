import { Suspense } from 'react';
import { Outlet, useNavigate } from 'react-router';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { app } from 'src/shared/firebase';

export function ProtectedRoute() {
  const navigate = useNavigate();
  const auth = getAuth(app);

  onAuthStateChanged(auth, (user) => {
    if (user) {
      const { uid } = user;
      localStorage.setItem('user', uid);
    } else {
      navigate('/login');
    }
  });

  return (
    <Suspense>
      <Outlet />
    </Suspense>
  );
}
