import type { Response } from 'src/shared/firebase/types';

export type AddResponseParams = Omit<Response, 'id' | 'createdAt'>;

export interface GetResponsesParams {
  formId?: string;
  filter?: {
    startDate: string;
    endDate: string;
  };
  sort?: string;
}
