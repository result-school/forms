import { createApi } from '@reduxjs/toolkit/query/react';
import { fakeBaseQuery } from '@reduxjs/toolkit/query';
import {
  collection,
  deleteDoc,
  doc,
  getDoc,
  getDocs,
  query,
  setDoc,
  where,
} from 'firebase/firestore';
import { COLLECTIONS, db } from 'src/shared/firebase';
import type { Form } from 'src/shared/firebase/types';

export const formsSlice = createApi({
  reducerPath: 'forms',
  baseQuery: fakeBaseQuery(),
  tagTypes: ['forms'],
  endpoints: (builder) => ({
    addForm: builder.mutation<string, Form>({
      async queryFn(data) {
        try {
          const docRef = doc(db, COLLECTIONS.forms, data.id);
          await setDoc(docRef, {
            userId: data.userId,
            title: data.title,
            description: data.description,
            questions: data.questions,
            createdAt: data.createdAt,
          });

          return { data: data.id };
        } catch (error) {
          return { error };
        }
      },
      invalidatesTags: ['forms'],
    }),
    deleteForm: builder.mutation<string, string>({
      async queryFn(id) {
        try {
          const docRef = doc(db, COLLECTIONS.forms, id);
          await deleteDoc(docRef);

          return { data: id };
        } catch (error) {
          return { error };
        }
      },
      invalidatesTags: ['forms'],
    }),
    getForm: builder.query<Form, string | undefined>({
      async queryFn(id) {
        try {
          const docRef = doc(db, COLLECTIONS.forms, id ?? '');
          const querySnapshot = await getDoc(docRef);

          const form = {
            ...querySnapshot.data(),
            id: querySnapshot.id,
          } as Form;

          return { data: form };
        } catch (error) {
          return { error };
        }
      },
      providesTags: ['forms'],
    }),
    getUserForms: builder.query<Form[], void>({
      async queryFn() {
        try {
          const userId = localStorage.getItem('user');
          const responsesRef = collection(db, COLLECTIONS.responses);
          const formsRef = collection(db, COLLECTIONS.forms);
          const formsByUserId = query(formsRef, where('userId', '==', userId));

          const [forms, responses] = await Promise.all([
            await getDocs(formsByUserId),
            await getDocs(responsesRef),
          ]);

          const result: Form[] = [];

          forms?.docs?.forEach((document) => {
            const formId = document.id;
            const responseCount = responses?.docs
              ?.map((item) => item.data())
              ?.filter((item) => item.form?.id === formId)
              ?.length;

            result.push({
              ...document.data(),
              id: formId,
              responseCount,
            } as Form);
          });

          return { data: result };
        } catch (error) {
          return { error };
        }
      },
      providesTags: ['forms'],
    }),
  }),
});
