import { createApi } from '@reduxjs/toolkit/query/react';
import { fakeBaseQuery } from '@reduxjs/toolkit/query';
import {
  doc, getDoc, setDoc, updateDoc,
} from 'firebase/firestore';
import { COLLECTIONS, auth, db } from 'src/shared/firebase';
import { createUserWithEmailAndPassword, signInWithEmailAndPassword } from 'firebase/auth';
import type { IProfile, LoginData, RegisterData } from 'src/shared/firebase/types';
import { formsSlice } from './formsSlice';

export const usersSlice = createApi({
  reducerPath: 'users',
  baseQuery: fakeBaseQuery(),
  tagTypes: ['users'],
  endpoints: (builder) => ({
    getProfile: builder.query<IProfile, void>({
      async queryFn() {
        try {
          const userId = localStorage.getItem('user') ?? '';
          const usersCollectionRef = doc(db, COLLECTIONS.users, userId);
          const data = await getDoc(usersCollectionRef);

          return { data: data.data() as IProfile };
        } catch (error) {
          return { error };
        }
      },
      providesTags: ['users'],
    }),
    setProfile: builder.mutation<string, IProfile>({
      async queryFn(data) {
        try {
          const userId = localStorage.getItem('user') ?? '';
          const docRef = doc(db, COLLECTIONS.users, userId);
          await updateDoc(docRef, { ...data });

          return { data: userId };
        } catch (error) {
          return { error };
        }
      },
      invalidatesTags: ['users'],
    }),
    registerUser: builder.mutation<string, RegisterData>({
      async queryFn(data) {
        try {
          const user = await createUserWithEmailAndPassword(auth, data.email, data.password);
          await setDoc(doc(db, COLLECTIONS.users, user.user?.uid), data);

          return { data: user.user?.uid };
        } catch (error) {
          return { error };
        }
      },
      invalidatesTags: ['users'],
      onQueryStarted: (_, api) => {
        api.queryFulfilled.then(() => {
          api.dispatch(
            formsSlice.util.invalidateTags(['forms']),
          );
        });
      },
    }),
    loginUser: builder.mutation<string, LoginData>({
      async queryFn(data) {
        try {
          const user = await signInWithEmailAndPassword(auth, data.email, data.password);

          return { data: user.user?.uid };
        } catch (error) {
          return { error };
        }
      },
      invalidatesTags: ['users'],
      onQueryStarted: (_, api) => {
        api.queryFulfilled.then(() => {
          api.dispatch(
            formsSlice.util.invalidateTags(['forms']),
          );
        });
      },
    }),
  }),
});
