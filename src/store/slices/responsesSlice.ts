import { createApi } from '@reduxjs/toolkit/query/react';
import { fakeBaseQuery } from '@reduxjs/toolkit/query';
import {
  addDoc, collection, doc, getDoc, getDocs, query, where,
} from 'firebase/firestore';
import { COLLECTIONS, db } from 'src/shared/firebase';
import { filterByDate, sortByDate } from 'src/shared/utils';
import type { Response } from 'src/shared/firebase/types';
import type { AddResponseParams, GetResponsesParams } from './types';
import { formsSlice } from './formsSlice';

export const responsesSlice = createApi({
  reducerPath: 'responses',
  baseQuery: fakeBaseQuery(),
  tagTypes: ['responses'],
  endpoints: (builder) => ({
    addResponse: builder.mutation<string, AddResponseParams>({
      async queryFn(data) {
        try {
          const collectionRef = collection(db, COLLECTIONS.responses);
          const docRef = await addDoc(collectionRef, {
            ...data,
            createdAt: new Date().toISOString(),
          });

          return { data: docRef.id };
        } catch (error) {
          return { error };
        }
      },
      invalidatesTags: ['responses'],
      onQueryStarted: (_, api) => {
        api.queryFulfilled.then(() => {
          api.dispatch(
            formsSlice.util.invalidateTags(['forms']),
          );
        });
      },
    }),
    getResponses: builder.query<Response[], GetResponsesParams>({
      async queryFn({ formId, sort, filter }) {
        try {
          const docRef = await getDocs(
            query(
              collection(db, COLLECTIONS.responses),
              where('form.id', '==', formId),
            ),
          );

          let result = [];
          // eslint-disable-next-line @typescript-eslint/no-shadow
          result = docRef.docs.map((doc) => ({
            ...doc.data(),
            id: doc.id,
          }) as Response);

          if (filter?.startDate || filter?.endDate) {
            result = filterByDate(result, filter);
          }

          if (sort) {
            result = sortByDate(result, sort);
          }

          return { data: result as Response[] };
        } catch (error) {
          return { error };
        }
      },
      providesTags: ['responses'],
    }),
    getSingleResponses: builder.query<Response, string>({
      async queryFn(id) {
        try {
          const usersCollectionRef = doc(db, COLLECTIONS.responses, id);
          const data = await getDoc(usersCollectionRef);

          return { data: data.data() as Response };
        } catch (error) {
          return { error };
        }
      },
      providesTags: ['responses'],
    }),
  }),
});
