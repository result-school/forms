import { configureStore } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';
import { formsSlice, responsesSlice, usersSlice } from './slices';

const rootReducer = combineReducers({
  [formsSlice.reducerPath]: formsSlice.reducer,
  [responsesSlice.reducerPath]: responsesSlice.reducer,
  [usersSlice.reducerPath]: usersSlice.reducer,
});

export const store = configureStore({
  reducer: rootReducer,
  devTools: true,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: false,
  }).concat([
    formsSlice.middleware,
    responsesSlice.middleware,
    usersSlice.middleware,
  ]),
});

export type RootState = ReturnType<typeof rootReducer>;
