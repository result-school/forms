import { filterByDate } from '../shared/utils/filterByDate';

const stubDates = {
  A: '2023-12-20T05:18:00.185Z',
  B: '2024-02-14T12:13:02.185Z',
  C: '2024-02-23T18:18:06.185Z',
};

describe('Фильтрация списка по дате', () => {
  const items = [
    { createdAt: stubDates.A },
    { createdAt: stubDates.B },
    { createdAt: stubDates.C },
  ];

  it('Правильно фильтрует элементы по начальной дате', () => {
    const filteredItems = filterByDate(items, { startDate: '2023-12-30', endDate: '' });
    expect(filteredItems).toEqual([
      { createdAt: stubDates.B },
      { createdAt: stubDates.C },
    ]);
  });

  it('Правильно фильтрует элементы по конечной дате', () => {
    const filteredItems = filterByDate(items, { startDate: '', endDate: '2024-02-20' });
    expect(filteredItems).toEqual([
      { createdAt: stubDates.A },
      { createdAt: stubDates.B },
    ]);
  });

  it('Правильно фильтрует элементы по начальной и конечной дате', () => {
    const filteredItems = filterByDate(items, { startDate: '2023-12-30', endDate: '2024-02-20' });
    expect(filteredItems).toEqual([
      { createdAt: stubDates.B },
    ]);
  });

  it('Возвращает элемент, если начальная или конечная дата совпадает с датой элемента', () => {
    const filteredItems = filterByDate(items, { startDate: '2023-12-20', endDate: '2024-02-23' });
    expect(filteredItems).toEqual(items);
  });

  it('Возвращает все элементы, если диапазон дат не указан', () => {
    const filteredItems = filterByDate(items, { startDate: '', endDate: '' });
    expect(filteredItems).toEqual(items);
  });
});
