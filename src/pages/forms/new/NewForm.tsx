import { collection, doc } from 'firebase/firestore';
import { Navigate } from 'react-router';
import { db } from 'src/shared/firebase';

export default function NewForm() {
  const newFormRef = doc(collection(db, 'forms'));

  return (
    <Navigate to={`/forms/${newFormRef.id}/edit`} replace />
  );
}
