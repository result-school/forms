import { lazy } from 'react';

export const NewForm = lazy(() => import('./NewForm'));
