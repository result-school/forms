import * as yup from 'yup';
import { Question } from 'src/shared/firebase/types';

export const validationSchema = yup.object().shape({
  answers: yup.array()
    .of(
      yup.object().shape({
        question: yup.object<Question>().shape({
          required: yup.boolean(),
        }),
        answer: yup.lazy((value) => (Array.isArray(value)
          ? yup.array().when('question', {
            is: (question: Question) => question?.required,
            then: (schema) => schema.min(1, 'Выберите хотя бы один вариант'),
          })
          : yup.string().when('question', {
            is: (question: Question) => question?.required,
            then: (schema) => schema.required('Обязательный вопрос'),
          })
        )),
      }),
    ),
});
