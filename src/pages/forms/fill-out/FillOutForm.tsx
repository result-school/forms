import {
  FieldArray, Form, Formik,
} from 'formik';
import { useState } from 'react';
import { useParams } from 'react-router';
import { QUESTION_TYPE, ResponseFormValues } from 'src/shared/firebase/types';
import {
  AnswerCard, Button, Description, Title, Tile, Modal, Loader,
} from 'src/shared/components';
import { responsesSlice, formsSlice } from 'src/store/slices';
import { validationSchema } from './validation';
import './styles.scss';

export default function FillOutForm() {
  const { formId } = useParams() as { formId: string };
  const { data, isLoading } = formsSlice.useGetFormQuery(formId);
  const [addResponse] = responsesSlice.useAddResponseMutation();
  const [successModal, setSuccessModal] = useState(false);

  const initialValues: ResponseFormValues = {
    answers: data?.questions.map((question) => ({
      question,
      answer: question.type === QUESTION_TYPE.checkbox ? [] : '',
    })) ?? [],
  };

  const handleSubmit = (values: ResponseFormValues) => {
    addResponse({
      form: {
        id: formId,
        title: data?.title ?? '',
        description: data?.description ?? '',
      },
      answers: values.answers,
    });
    setSuccessModal(true);
  };

  if (isLoading) return <Loader />;

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ values, isValid, dirty }) => (
          <Form className="fill-out-form">
            <Tile>
              <Title>{data?.title}</Title>
              <Description>{data?.description}</Description>
            </Tile>
            <FieldArray
              name="answers"
              render={() => values.answers.map((item, index) => (
                <AnswerCard
                  key={index}
                  index={index}
                  question={item.question}
                />
              ))}
            />
            <div className="buttons">
              <Button type="submit" disabled={!dirty || !isValid}>
                Отправить форму
              </Button>
            </div>
          </Form>
        )}
      </Formik>
      <Modal
        open={successModal}
        title="Спасибо за участие!"
        description="Форма успешно отправлена"
        withReturnButton
      />
    </>
  );
}
