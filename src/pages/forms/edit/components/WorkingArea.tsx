import { Field, useFormikContext } from 'formik';
import type { FormValues } from 'src/shared/firebase/types';
import { TextInput } from 'src/shared/components';
import { QuestionList } from './QuestionList';

export function WorkingArea() {
  const { errors } = useFormikContext<FormValues>();

  return (
    <div className="working-area">
      <div>
        <Field
          name="title"
          placeholder="Название формы"
          className="title-input"
          component={TextInput}
          error={errors.title}
          center
          light
        />
        <Field
          name="description"
          placeholder="Описание"
          className="description-input"
          component={TextInput}
          center
          light
        />
      </div>
      <QuestionList />
    </div>
  );
}
