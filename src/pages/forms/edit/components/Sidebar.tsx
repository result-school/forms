import { useFormikContext } from 'formik';
import { Button, Tile, OptionCard } from 'src/shared/components';
import type { FormValues } from 'src/shared/firebase/types';
import { options } from './constants';

interface SidebarProps {
  onFormDelete: () => void;
}

export function Sidebar({ onFormDelete }: SidebarProps) {
  const { values } = useFormikContext<FormValues>();

  return (
    <Tile className="sidebar">
      <div className="options">
        {options.map((item, index) => (
          <OptionCard
            icon={item.icon()}
            label={item.label}
            key={index}
            type={item.type}
            id={item.id}
          />
        ))}
      </div>
      <div className="buttons">
        <Button fullWidth onClick={onFormDelete}>
          Удалить
        </Button>
        <Button
          fullWidth
          type="submit"
          disabled={!values.questions.length}
        >
          Сохранить
        </Button>
      </div>
    </Tile>
  );
}
