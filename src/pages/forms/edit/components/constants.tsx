import { Bars3BottomLeftIcon, CheckCircleIcon } from '@heroicons/react/24/outline';
import { ReactNode } from 'react';
import { QUESTION_TYPE } from 'src/shared/firebase/types';
import TextIcon from 'src/shared/assets/text-icon.svg?react';

interface Option {
  id: string,
  type: string;
  label: string;
  icon: () => ReactNode;
}

export const options: Option[] = [
  {
    id: '1',
    type: QUESTION_TYPE.input,
    label: 'Короткий текст',
    icon: () => (<TextIcon />),
  },
  {
    id: '2',
    type: QUESTION_TYPE.textfield,
    label: 'Длинный текст',
    icon: () => (<Bars3BottomLeftIcon />),
  },
  {
    id: '3',
    type: QUESTION_TYPE.radio,
    label: 'Выбор ответа',
    icon: () => (<CheckCircleIcon />),
  },
];
