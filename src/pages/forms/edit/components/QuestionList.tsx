import { FieldArray, useFormikContext } from 'formik';
import {
  useCallback, useState, useRef, useEffect,
} from 'react';
import { useDrop } from 'react-dnd';
import { QuestionCard } from 'src/shared/components';
import { generateId } from 'src/shared/utils';
import type { FormValues, Question } from 'src/shared/firebase/types';

export function QuestionList() {
  const { values, setFieldValue } = useFormikContext<FormValues>();
  const [rect, setRect] = useState<DOMRect>(new DOMRect());
  const workAreaRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    const coordData = workAreaRef.current?.getBoundingClientRect();
    setRect(coordData ?? new DOMRect());
  }, []);

  function addQuestionToWorkingArea(item: Question): void {
    const droppedId = generateId(200);
    const itemToAdd = {
      ...item,
      id: droppedId,
    };
    values.questions.push(itemToAdd);
    setFieldValue('questions', values.questions);
  }
  const findQuestion = useCallback((id: string) => {
    const question = values.questions.filter((q) => q.id === id)[0];
    const index = values.questions.indexOf(question);
    return {
      question,
      index,
    };
  }, [values.questions]);

  const moveCard = useCallback((id: string, atIndex: number) => {
    const { index } = findQuestion(id);
    const [newOrder] = values.questions.splice(index, 1);
    values.questions.splice(atIndex, 0, newOrder);
    setFieldValue('questions', [...values.questions]);
  }, [findQuestion, values.questions]);

  // from sidebar
  const [, drop] = useDrop(() => ({
    accept: 'field',
    drop: (item: Question) => addQuestionToWorkingArea(item),
    collect: (monitor) => ({
      isOver: !!monitor.isOver(),
    }),
  }), [values.questions]);

  const deleteCard = useCallback((id: string): void => {
    values.questions = values.questions.filter((item) => item.id !== id);
    setFieldValue('questions', [...values.questions]);
  }, [values.questions]);

  return (
    <div ref={workAreaRef} className="h-full">
      <FieldArray
        name="questions"
        render={() => (
          <div ref={drop} className="h-full">
            {values.questions.length > 0 ? (
              <div className="questions">
                {values.questions.map((item, index) => (
                  <QuestionCard
                    key={item.id}
                    question={item}
                    index={index}
                    id={item.id}
                    onDelete={(id) => deleteCard(id)}
                    coordData={rect}
                    moveCard={moveCard}
                    findQuestion={findQuestion}
                  />
                ))}
              </div>
            ) : (
              <div className="questions-empty">
                Добавьте вопрос в это поле
              </div>
            )}
          </div>
        )}
      />
    </div>
  );
}
