import { lazy } from 'react';

export const EditForm = lazy(() => import('./EditForm'));
