import { Form, Formik } from 'formik';
import { useState } from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { TouchBackend } from 'react-dnd-touch-backend';
import { useParams } from 'react-router';
import { useNavigate } from 'react-router-dom';
import { Loader, Modal } from 'src/shared/components';
import type { FormValues } from 'src/shared/firebase/types';
import { formsSlice } from 'src/store/slices';
import { Sidebar } from './components/Sidebar';
import { WorkingArea } from './components/WorkingArea';
import { validationSchema } from './validation';
import './styles.scss';

const isMobile = /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
const DndBackend = isMobile ? TouchBackend : HTML5Backend;
const baseURL = import.meta.env.PROD ? import.meta.env.BASE_URL : 'http://localhost:5173';

export default function EditForm() {
  const navigate = useNavigate();
  const { formId } = useParams() as { formId: string };
  const [successModal, setSuccessModal] = useState(false);

  const { data, isLoading } = formsSlice.useGetFormQuery(formId);
  const [addForm, { isError }] = formsSlice.useAddFormMutation();
  const [deleteForm] = formsSlice.useDeleteFormMutation();

  const initialValues = {
    title: data?.title ?? '',
    description: data?.description ?? '',
    questions: data?.questions ? [...data.questions] : [],
  };

  const handleSubmit = async (
    values: FormValues,
  ) => {
    await addForm({
      id: formId,
      userId: localStorage.getItem('user') ?? '',
      title: values.title,
      description: values.description,
      questions: values.questions,
      createdAt: new Date().toISOString(),
    });

    if (!isError) {
      setSuccessModal(true);
    }
  };

  const handleDelete = async () => {
    if (data) {
      await deleteForm(formId);
    }

    navigate('/');
  };

  if (isLoading) return <Loader />;

  return (
    <DndProvider backend={DndBackend}>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        validateOnChange={false}
        validateOnBlur={false}
        onSubmit={handleSubmit}
      >
        <Form className="edit-form-wrapper">
          <Sidebar onFormDelete={handleDelete} />
          <WorkingArea />
        </Form>
      </Formik>
      <Modal
        open={successModal}
        onClose={() => setSuccessModal(false)}
        title="Форма успешно сохранена!"
        description="Скопируйте ссылку, чтобы отправить своим друзьям:"
        link={`${baseURL}/forms/${formId}`}
        withReturnButton
      />
    </DndProvider>
  );
}
