import * as yup from 'yup';
import { QUESTION_TYPE } from 'src/shared/firebase/types';

export const validationSchema = yup.object().shape({
  title: yup.string().required('Введите название формы'),
  questions: yup.array()
    .of(
      yup.object().shape({
        title: yup.string().required('Введите вопрос'),
        answers: yup.array()
          .when('type', {
            is: (type: QUESTION_TYPE) => [
              QUESTION_TYPE.checkbox,
              QUESTION_TYPE.radio,
            ].includes(type),
            then: (schema) => schema.of(
              yup.string().required('Введите вариант ответа'),
            ).min(1, 'Добавьте хотя бы один вариант ответа'),
          }),
      }),
    )
    .min(1, 'Добавьте хотя бы один вопрос'),
});
