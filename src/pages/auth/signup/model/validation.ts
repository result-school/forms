import { useState } from 'react';
import {
  ISigninForm, IErrorForm, IError, IValidateResult, IValidateParam,
} from './types';
import {
  textValidator, emailValidator, passwordValidator, passwordConfirmValidator,
} from './validators';

const touchErrors = (errors: IErrorForm) => Object.entries(errors)
  .reduce<IErrorForm>((acc, [field, fieldError]) => {
  acc[field] = {
    ...fieldError,
    dirty: true,
  };
  return acc;
}, {});

export const useSigninFormValidator = (inputs: ISigninForm) => {
  const [errors, setErrors] = useState<IErrorForm>({
    name: {
      dirty: false,
      error: false,
      message: '',
    },
    surname: {
      dirty: false,
      error: false,
      message: '',
    },
    email: {
      dirty: false,
      error: false,
      message: '',
    },
    password: {
      dirty: false,
      error: false,
      message: '',
    },
    passwordConfirm: {
      dirty: false,
      error: false,
      message: '',
    },
  });

  const validateForm = ({
    formData = inputs, inputName, err = errors, forceTouchErrors = false,
  }: IValidateParam): IValidateResult => {
    let isValid = true;
    let newErrors = JSON.parse(JSON.stringify(err));

    if (forceTouchErrors) {
      newErrors = touchErrors(err);
    }

    if (newErrors.name.dirty && (inputName ? inputName === 'name' : true)) {
      const nameErrMessage = textValidator(formData.name);
      newErrors.name.error = !!nameErrMessage;
      newErrors.name.message = nameErrMessage;
      if (newErrors.name.error) isValid = false;
    }
    if (newErrors.surname.dirty && (inputName ? inputName === 'surname' : true)) {
      const surnameErrMessage = textValidator(formData.surname);
      newErrors.surname.error = !!surnameErrMessage;
      newErrors.surname.message = surnameErrMessage;
      if (newErrors.surname.error) isValid = false;
    }
    if (newErrors.email.dirty && (inputName ? inputName === 'email' : true)) {
      const emailErrMessage = emailValidator(formData.email);
      newErrors.email.error = !!emailErrMessage;
      newErrors.email.message = emailErrMessage;
      if (newErrors.email.error) isValid = false;
    }
    if (newErrors.password.dirty && (inputName ? inputName === 'password' : true)) {
      const passwordErrMessage = passwordValidator(formData.password);
      newErrors.password.error = !!passwordErrMessage;
      newErrors.password.message = passwordErrMessage;
      if (newErrors.password.error) isValid = false;
    }
    if (newErrors.passwordConfirm.dirty && (inputName ? inputName === 'passwordConfirm' : true)) {
      const passwordConfirmErrMessage = passwordConfirmValidator(
        formData.passwordConfirm,
        formData,
      );
      newErrors.passwordConfirm.error = !!passwordConfirmErrMessage;
      newErrors.passwordConfirm.message = passwordConfirmErrMessage;
      if (newErrors.passwordConfirm.error) isValid = false;
    }

    setErrors(newErrors);
    return {
      isValid,
      errors: newErrors,
    };
  };

  const handleBlur = (e: React.ChangeEvent<HTMLFormElement>) => {
    const inputName: string = e.target.name;
    const inputError: IError = errors[inputName];
    if (inputError?.dirty) return;

    const updatedErrors: IErrorForm = {
      ...errors,
      [inputName]: {
        ...errors[inputName],
        dirty: true,
      },
    };

    validateForm({ formData: inputs, inputName, err: updatedErrors });
  };

  return {
    validateForm,
    handleBlur,
    errors,
  };
};
