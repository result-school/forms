export interface ISigninForm {
  name: string;
  surname: string;
  email: string;
  password: string;
  passwordConfirm: string;
}

export interface IError {
  dirty: boolean;
  error: boolean;
  message: string;
}
export interface IErrorForm {
  [key: string]: IError;
}

export interface IValidateResult {
  isValid: boolean,
  errors: IErrorForm
}

export interface IValidateParam {
  formData: ISigninForm,
  inputName?: string,
  err: IErrorForm,
  forceTouchErrors?: boolean
}

export type InputEvent = React.ChangeEvent<HTMLInputElement>;
