import { EMAIL_REGEXP } from './constants';
import { ISigninForm } from './types';

export const textValidator = (text: string) => {
  if (!text.trim()) {
    return 'Поле не должно быть пустым';
  } else return '';
};

export const emailValidator = (email: string) => {
  if (!email.trim()) {
    return 'Поле не должно быть пустым';
  } else if (!EMAIL_REGEXP.test(email)) {
    return 'Введите корректную почту';
  } else return '';
};

export const passwordValidator = (password: string) => {
  if (!password.trim()) {
    return 'Поле не должно быть пустым';
  } else if (password.trim().length < 6) {
    return 'Количество символов должно быть не меньше 6';
  } else return '';
};

export const passwordConfirmValidator = (password_confirm: string, form: ISigninForm) => {
  if (!password_confirm.trim()) {
    return 'Поле не должно быть пустым';
  } else if (password_confirm.trim().length < 6) {
    return 'Количество символов должно быть не меньше 6';
  } else if (password_confirm !== form.password) {
    return 'Пароли не совпадают';
  } else return '';
};
