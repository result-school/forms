import { SignupForm } from './ui/SignupForm';

function Signup() {
  return <SignupForm />;
}

export default Signup;
