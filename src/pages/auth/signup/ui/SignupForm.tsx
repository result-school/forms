import { ChangeEvent, FormEvent, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import {
  Button, Title, TextInput, PasswordInput,
} from 'src/shared/components';
import { usersSlice } from 'src/store/slices';
import { useSigninFormValidator } from '../model/validation';
import './styles.css';

export function SignupForm() {
  const [inputs, setInputs] = useState({
    name: '',
    surname: '',
    email: '',
    password: '',
    passwordConfirm: '',
  });
  const [isValidForm, setIsValidForm] = useState(false);
  const [submitError, setSubmitError] = useState('');
  const [inputType, setInputType] = useState('password');
  const [inputTypeConfirm, setInputTypeConfirm] = useState('password');
  const [isShowedPassword, setIsShowedPassword] = useState(false);
  const [isShowedPasswordConfirm, setIsShowedPasswordConfirm] = useState(false);
  const { errors, validateForm, handleBlur } = useSigninFormValidator(inputs);
  const [registerUser] = usersSlice.useRegisterUserMutation();
  const navigate = useNavigate();

  const handleChange = (event: ChangeEvent<HTMLFormElement>): void => {
    const inputName = event.target.name;
    setInputs((prevState) => {
      const newState = {
        ...prevState,
        [inputName]: event.target.value,
      };
      if (errors[inputName].dirty) {
        const { isValid } = validateForm({ formData: newState, inputName, err: errors });
        setIsValidForm(isValid);
      }
      return newState;
    });
  };

  const handleSubmit = async (event: FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault();
    const { isValid } = validateForm({ formData: inputs, err: errors, forceTouchErrors: true });
    setIsValidForm(isValid);

    if (isValid) {
      await registerUser(inputs)
        .unwrap()
        .then(() => {
          navigate('/');
        })
        .catch((error) => {
          const errorCode = error.code;
          switch (errorCode) {
            case 'auth/email-already-in-use':
              setSubmitError('Данная почта уже используется.');
              break;
            case 'auth/invalid-email':
              setSubmitError('Адрес электронной почты недействителен.');
              break;
            case 'auth/weak-password':
              setSubmitError('Пароль недостаточно надежный.');
              break;
            default:
              setSubmitError(error.message);
          }
        });
    } else {
      setSubmitError('Форма не валидна');
    }
  };

  const handleToggle = (event: React.MouseEvent<HTMLButtonElement>, show: boolean): void => {
    event.preventDefault();
    setIsShowedPassword(!show);
    if (show === true) {
      setInputType('password');
    } else {
      setInputType('text');
    }
  };
  const handleToggleConfirm = (event: React.MouseEvent<HTMLButtonElement>, show: boolean): void => {
    event.preventDefault();
    setIsShowedPasswordConfirm(!show);
    if (show === true) {
      setInputTypeConfirm('password');
    } else {
      setInputTypeConfirm('text');
    }
  };

  return (
    <form
      className="form gap-4"
      onChange={handleChange}
      onBlur={handleBlur}
      onSubmit={handleSubmit}
    >
      <Title>Регистрация</Title>
      <TextInput
        name="name"
        type="text"
        required={false}
        placeholder="Имя"
        disabled={false}
        error={errors.name.dirty && errors.name.message !== '' ? errors.name.message : ''}
      />
      <TextInput
        name="surname"
        type="text"
        required={false}
        placeholder="Фамилия"
        disabled={false}
        error={errors.surname.dirty && errors.surname.message !== '' ? errors.surname.message : ''}
      />
      <TextInput
        name="email"
        type="text"
        required={false}
        placeholder="Электронная почта"
        disabled={false}
        error={errors.email.dirty && errors.email.message !== '' ? errors.email.message : ''}
      />
      <PasswordInput
        name="password"
        type={inputType}
        required={false}
        placeholder="Пароль"
        disabled={false}
        show={isShowedPassword}
        error={errors.password.dirty && errors.password.message !== '' ? errors.password.message : ''}
        onClick={(e) => handleToggle(e, isShowedPassword)}
      />
      <PasswordInput
        name="passwordConfirm"
        type={inputTypeConfirm}
        required={false}
        placeholder="Повторите пароль"
        disabled={false}
        show={isShowedPasswordConfirm}
        error={errors.passwordConfirm.dirty && errors.passwordConfirm.message !== '' ? errors.passwordConfirm.message : ''}
        onClick={(e) => handleToggleConfirm(e, isShowedPasswordConfirm)}
      />
      <Button type="submit" disabled={!isValidForm}>
        Зарегистрироваться
      </Button>
      {
        submitError.length > 0 && (<p className="error-text">{submitError}</p>)
      }
      <div className="mx-auto text-sm">
        <span className="text-sm text-center">
          Есть аккаунт?
          <Link
            to="/login"
            className="ml-1 text-theme font-bold hover:opacity-80"
          >
            Войти
          </Link>
        </span>
      </div>
    </form>
  );
}
