import { useRef, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { Title, Button, TextInput } from 'src/shared/components';
import { usersSlice } from 'src/store/slices';
import { validationLoginForm } from '../model/validation';
import './styles.css';

export function LoginForm() {
  const inputs = useRef({
    email: '',
    password: '',
  });
  const [errors, setErrors] = useState({
    email: '',
    password: '',
  });
  const [loginUser] = usersSlice.useLoginUserMutation();
  const [isDisabled, setIsDisabled] = useState(true);
  const [submitError, setSubmitError] = useState('');
  const navigate = useNavigate();

  const handleChange = (event: React.ChangeEvent<HTMLFormElement>): void => {
    inputs.current = {
      ...inputs.current,
      [event.target.name]: event.target.value,
    };
    validationLoginForm(inputs.current, setIsDisabled);
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
    event.preventDefault();
    validationLoginForm(inputs.current, setIsDisabled);
    setErrors(validationLoginForm(inputs.current, setIsDisabled));

    if (!isDisabled) {
      loginUser(inputs.current)
        .then(() => {
          navigate('/');
        })
        .catch((error) => {
          const errorCode = error.code;
          switch (errorCode) {
            case 'auth/invalid-credential':
              setSubmitError('Введен неверная почта и/или пароль.');
              break;
            case 'auth/invalid-email':
              setSubmitError('Адрес электронной почты недействителен.');
              break;
            case 'auth/weak-password':
              setSubmitError('Пароль недостаточно надежный.');
              break;
            default:
              setSubmitError(error.message);
          }
        });
    }
  };

  return (
    <form
      className="form"
      onChange={handleChange}
      onSubmit={handleSubmit}
    >
      <Title>Вход</Title>
      <TextInput
        name="email"
        type="text"
        required
        placeholder="Электронная почта"
        disabled={false}
        error={errors.email}
      />
      <TextInput
        name="password"
        type="password"
        required
        placeholder="Пароль"
        disabled={false}
        error={errors.password}
      />
      <Button type="submit" disabled={isDisabled}>Войти</Button>
      {
        submitError.length > 0 && <p className="error-text">{submitError}</p>
      }
      <div className="mx-auto text-sm">
        <span className="text-sm text-center">
          Нет аккаунта?
          <Link
            to="/signup"
            className="ml-1 text-theme font-bold hover:opacity-80"
          >
            Зарегистрироваться
          </Link>
        </span>
      </div>
    </form>
  );
}
