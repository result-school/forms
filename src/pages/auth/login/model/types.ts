export interface ILoginForm {
  email: string;
  password: string;
}

export type InputEvent = React.ChangeEvent<HTMLInputElement>;
