import { ILoginForm } from './types';
import { EMAIL_REGEXP } from './constants';

export const validationLoginForm = (
  inputFields: ILoginForm,
  setDisabledButton: React.Dispatch<React.SetStateAction<boolean>>,
) => {
  const validationErrors: ILoginForm = {
    email: '',
    password: '',
  };

  setDisabledButton(true);
  if (!inputFields.email.trim()) {
    validationErrors.email = 'Поле не должно быть пустым';
  } else if (!EMAIL_REGEXP.test(inputFields.email)) {
    validationErrors.email = 'Введите корректную почту';
  }

  if (!inputFields.password.trim()) {
    validationErrors.password = 'Поле не должно быть пустым';
  } else if (inputFields.password.trim().length < 6) {
    validationErrors.password = 'Количество символов должно быть не меньше 6';
  } else {
    setDisabledButton(false);
  }
  return validationErrors;
};
