import { Route, Routes } from 'react-router-dom';
import { ProtectedRoute, PublicRoute, UnauthorizedOnlyRoute } from 'src/shared/routing';
import { AuthLayout, MainLayout } from 'src/shared/layouts';
import { Home } from './home';
import { Login } from './auth/login';
import { Signup } from './auth/signup';
import { Profile } from './profile';
import { ProfileEdit } from './profile/edit';
import { NewForm } from './forms/new';
import { EditForm } from './forms/edit';
import { FillOutForm } from './forms/fill-out';
import { ResponseList } from './responses/list';
import { ResponseDetail } from './responses/detail';

export function Pages() {
  return (
    <Routes>
      <Route element={<AuthLayout />}>
        <Route element={<UnauthorizedOnlyRoute />}>
          <Route path="login" element={<Login />} />
          <Route path="signup" element={<Signup />} />
        </Route>
      </Route>
      <Route element={<MainLayout />}>
        <Route element={<PublicRoute />}>
          <Route path="forms/:formId" element={<FillOutForm />} />
        </Route>
        <Route element={<ProtectedRoute />}>
          <Route index element={<Home />} />
          <Route path="me" element={<Profile />} />
          <Route path="me/edit" element={<ProfileEdit />} />
          <Route path="forms/new" element={<NewForm />} />
          <Route path="forms/:formId/edit" element={<EditForm />} />
          <Route path="forms/:formId/responses" element={<ResponseList />} />
          <Route path="forms/:formId/responses/:responseId" element={<ResponseDetail />} />
        </Route>
      </Route>
    </Routes>
  );
}
