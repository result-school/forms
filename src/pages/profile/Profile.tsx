import { useNavigate } from 'react-router-dom';
import { usersSlice } from 'src/store/slices';
import { Loader } from '../../shared/components';
import './styles.scss';

export default function Profile() {
  const navigate = useNavigate();
  const { data, isLoading } = usersSlice.useGetProfileQuery();

  return (
    <div className="me">
      <svg className="me-edit" onClick={() => navigate('/me/edit')} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none">
        <path d="M9.65661 17L6.99975 17L6.99975 14M6.10235 14.8974L17.4107 3.58902C18.1918 2.80797 19.4581 2.80797 20.2392 3.58902C21.0202 4.37007 21.0202 5.6364 20.2392 6.41745L8.764 17.8926C8.22794 18.4287 7.95992 18.6967 7.6632 18.9271C7.39965 19.1318 7.11947 19.3142 6.8256 19.4723C6.49475 19.6503 6.14115 19.7868 5.43395 20.0599L3 20.9998L3.78312 18.6501C4.05039 17.8483 4.18403 17.4473 4.3699 17.0729C4.53497 16.7404 4.73054 16.424 4.95409 16.1276C5.20582 15.7939 5.50466 15.4951 6.10235 14.8974Z" />
      </svg>
      {isLoading && <Loader />}
      {!isLoading && data && (
        <>
          {data.url
            ? <img src={data.url} alt="" className="me-avatar" />
            : (
              <svg className="me-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none">
                <path d="M8 14C8 14 9.5 16 12 16C14.5 16 16 14 16 14M15 9H15.01M9 9H9.01M22 12C22 17.5228 17.5228 22 12 22C6.47715 22 2 17.5228 2 12C2 6.47715 6.47715 2 12 2C17.5228 2 22 6.47715 22 12ZM15.5 9C15.5 9.27614 15.2761 9.5 15 9.5C14.7239 9.5 14.5 9.27614 14.5 9C14.5 8.72386 14.7239 8.5 15 8.5C15.2761 8.5 15.5 8.72386 15.5 9ZM9.5 9C9.5 9.27614 9.27614 9.5 9 9.5C8.72386 9.5 8.5 9.27614 8.5 9C8.5 8.72386 8.72386 8.5 9 8.5C9.27614 8.5 9.5 8.72386 9.5 9Z" stroke="#000000" />
              </svg>
            )}
          <div className="me-fio">{ `${data.name} ${data.surname}` }</div>
          <div className="me-email">{ data.email }</div>
        </>
      )}
    </div>
  );
}
