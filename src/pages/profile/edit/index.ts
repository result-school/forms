import { lazy } from 'react';

export const ProfileEdit = lazy(() => import('./ProfileEdit'));
