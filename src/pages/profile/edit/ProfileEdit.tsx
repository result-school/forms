import {
  useEffect, useState, FormEventHandler, FormEvent,
} from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, TextInput } from 'src/shared/components';
import { usersSlice } from 'src/store/slices';
import { IProfile } from 'src/shared/firebase/types';
import './styles.scss';

export default function ProfileEdit() {
  const navigate = useNavigate();
  const [inputs, setInputs] = useState<IProfile | undefined>(undefined);
  const [isValidForm, setIsValidForm] = useState<boolean>(false);
  const [photoFile, setPhotoFile] = useState<File | undefined>(undefined);
  const { data } = usersSlice.useGetProfileQuery();
  const [setProfile] = usersSlice.useSetProfileMutation();
  const [isDisabled, setIsDisabled] = useState<boolean>(false);

  useEffect(() => {
    setInputs(data as IProfile);
  }, [data]);

  async function getBase64(file: File): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        resolve(reader.result as string);
      };
      reader.onerror = reject;
    });
  }

  const handleChange: FormEventHandler<HTMLFormElement> = async (
    event: FormEvent<HTMLFormElement>,
  ) => {
    const inputName = (event.target as HTMLInputElement).name;

    if (inputName === 'url') {
      const file = (event.target as HTMLInputElement).files?.[0];
      if (file) {
        setPhotoFile(file);

        const res = await getBase64(file);
        setInputs((prevState) => {
          const newState = {
            ...prevState,
            [inputName]: res,
          };
          setIsValidForm(newState?.name !== '' && newState?.surname !== '' && newState?.email !== '');
          return newState;
        });
      }
    } else {
      setInputs((prevState) => {
        const newState = {
          ...prevState,
          [inputName]: (event.target as HTMLInputElement | HTMLTextAreaElement).value,
        };
        setIsValidForm(newState?.name !== '' && newState?.surname !== '' && newState?.email !== '');
        return newState;
      });
    }
  };

  const removeAvatar = () => {
    setInputs({ ...inputs, url: '' });
    setPhotoFile(undefined);
  };

  const handleSubmit: FormEventHandler<HTMLFormElement> = async (event) => {
    event.preventDefault();
    setIsDisabled(true);

    if (isValidForm && inputs) {
      await setProfile(inputs);
      setIsDisabled(false);
      navigate('/me');
    } else {
      console.error('Form is invalid');
    }
  };

  return (
    <div className="me">
      <svg className="me-success" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none">
        <circle cx="12" cy="12" r="10" stroke={`${!isValidForm ? 'grey' : '#07e104'}`} />
        <path d="M8.5 12.5L10.5 14.5L15.5 9.5" stroke={`${!isValidForm ? 'grey' : '#07e104'}`} />
      </svg>
      {inputs?.url
        ? (
          <div className="me-avatar__preview">
            <img src={inputs?.url} alt="" />
            <div className="me-avatar__preview-name">{ photoFile?.name }</div>
            <svg
              viewBox="0 0 14 14"
              xmlns="http://www.w3.org/2000/svg"
              className="me-avatar__preview-remove"
              onClick={removeAvatar}
            >
              <path
                d="M12.5164 0.454545L13.5455 1.48363L8.0291 7L13.5455 12.5164L12.5164 13.5455L7 8.02909L1.48364 13.5455L0.454545 12.5164L5.9709 7L0.454545 1.48363L1.48364 0.454545L7 5.97091L12.5164 0.454545Z"
              />
            </svg>
          </div>
        )
        : (
          <svg className="me-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none">
            <path
              d="M8 14C8 14 9.5 16 12 16C14.5 16 16 14 16 14M15 9H15.01M9 9H9.01M22 12C22 17.5228 17.5228 22 12 22C6.47715 22 2 17.5228 2 12C2 6.47715 6.47715 2 12 2C17.5228 2 22 6.47715 22 12ZM15.5 9C15.5 9.27614 15.2761 9.5 15 9.5C14.7239 9.5 14.5 9.27614 14.5 9C14.5 8.72386 14.7239 8.5 15 8.5C15.2761 8.5 15.5 8.72386 15.5 9ZM9.5 9C9.5 9.27614 9.27614 9.5 9 9.5C8.72386 9.5 8.5 9.27614 8.5 9C8.5 8.72386 8.72386 8.5 9 8.5C9.27614 8.5 9.5 8.72386 9.5 9Z"
              stroke="#000000"
            />
          </svg>
        )}
      <form
        className="me-form"
        onChange={handleChange}
        onSubmit={handleSubmit}
      >
        <label htmlFor="avatar" className="me-loader">
          <span>
            Загрузить аватар пользователя
            <em>до 500 кб, форматы: png, jpg, svg</em>
          </span>
          <input id="avatar" type="file" name="url" accept=".png,.jpeg,.jpg,.svg" />
        </label>
        <TextInput
          label="Имя"
          name="name"
          type="text"
          required
          disabled={false}
          error={inputs?.name === '' ? 'Поле не должно быть пустым' : ''}
          value={inputs?.name}
        />
        <TextInput
          label="Фамилия"
          name="surname"
          type="text"
          required
          disabled={false}
          error={inputs?.surname === '' ? 'Поле не должно быть пустым' : ''}
          value={inputs?.surname}
        />
        <TextInput
          label="Электронная почта"
          name="email"
          type="email"
          required
          disabled={false}
          error={inputs?.email === '' ? 'Поле не должно быть пустым' : ''}
          value={inputs?.email}
        />
        <Button type="submit" disabled={!isValidForm || isDisabled}>
          Сохранить
        </Button>
      </form>
    </div>
  );
}
