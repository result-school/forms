import { lazy } from 'react';

export const ResponseDetail = lazy(() => import('./ResponseDetail'));
