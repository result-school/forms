import { NavLink } from 'react-router-dom';
import { Description, Loader, Title } from 'src/shared/components';
import { useParams } from 'react-router';
import { responsesSlice } from '../../../store/slices';
import './style.scss';

export default function ResponseDetail() {
  const { responseId } = useParams() as { responseId: string };
  const { data: single, isLoading } = responsesSlice.useGetSingleResponsesQuery(responseId);
  console.log('single', single);

  return (
    <div className="response">
      {isLoading && <Loader />}
      {single
        && (
          <>
            <div className="response-title">
              <Title>{ single.form.title }</Title>
              {single.form.description && <Description>{ single.form.description }</Description>}
            </div>
            {single.answers.length
              && single.answers.map((item) => (
                <fieldset className="response-item" key={item.question.id}>
                  <legend className="response-question">{ item.question.title }</legend>
                  {Array.isArray(item.answer)
                    ? item.answer.map((el) => (
                      <figcaption className="response-answer" key={el}>{ el }</figcaption>
                    ))
                    : <figcaption className="response-answer">{ item.answer }</figcaption>}
                </fieldset>
              ))}
            <div className="response-bottom">
              <NavLink to={`/forms/${single.form.id}`} className="button">Перейти к форме</NavLink>
              <NavLink to={`/forms/${single.form.id}/edit`} className="button">Редактировать форму</NavLink>
            </div>
          </>
        )}
    </div>
  );
}
