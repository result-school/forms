import dayjs from 'dayjs';
import * as xlsx from 'xlsx';
import { ChangeEvent, useState } from 'react';
import { useParams } from 'react-router';
import { NavLink, useSearchParams } from 'react-router-dom';
import {
  Button, DateInput, Select, Loader,
} from 'src/shared/components';
import { formsSlice, responsesSlice } from 'src/store/slices';
import './styles.scss';

export default function ResponseList() {
  const { formId } = useParams<{ formId: string }>();
  const [searchParams, setSearchParams] = useSearchParams();
  const sort = searchParams.get('sort') || '';
  const startDate = searchParams.get('startDate') || '';
  const endDate = searchParams.get('endDate') || '';

  const { data: form } = formsSlice.useGetFormQuery(formId);
  const { data: responses, isLoading } = responsesSlice.useGetResponsesQuery({
    filter: { startDate, endDate },
    formId,
    sort,
  }, { refetchOnMountOrArgChange: true });

  const [dates, setDates] = useState<{
    start: string;
    end: string;
  }>({ start: startDate, end: endDate });

  const handleSort = (event: ChangeEvent<HTMLSelectElement>): void => {
    setSearchParams({ sort: event.target.value, startDate, endDate });
  };

  const handleDateChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setDates((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };

  const handleExcelExport = () => {
    const workbook = xlsx.utils.book_new();
    const worksheet = xlsx.utils.json_to_sheet(responses?.map((response) => {
      const answersMap: { [key: string]: string } = {};

      response.answers.forEach(({ answer, question }) => {
        answersMap[question.title] = Array.isArray(answer) ? answer.join(', ') : answer;
      });

      return {
        'Дата и время отклика': new Date(response.createdAt),
        ...answersMap,
      };
    }) ?? []);

    xlsx.utils.book_append_sheet(workbook, worksheet, 'Отклики');
    xlsx.writeFile(workbook, `${form?.title ?? 'undefined'}.xlsx`);
  };

  const handleSubmit = (event: ChangeEvent<HTMLFormElement>): void => {
    event.preventDefault();
    setSearchParams({
      sort,
      startDate: dates.start,
      endDate: dates.end,
    });
  };

  return (
    <div>
      {isLoading && <Loader />}
      {form && (
        <h1 className="responses__title">
          {`Отклики на форму: ${form.title}`}
        </h1>
      )}
      {!isLoading && (
        <>
          <div className="responses__top">
            <form
              className="responses__form"
              onSubmit={handleSubmit}
            >
              <div className="responses__sort">
                <Select
                  value={sort}
                  onChange={handleSort}
                  options={{
                    ascDate: 'По дате создания по возрастанию',
                    descDate: 'По дате создания по убыванию',
                    ascTime: 'По времени создания по возрастанию',
                    descTime: 'По времени создания по убыванию',
                  }}
                />
              </div>
              <div className="responses__filter">
                <DateInput
                  name="start"
                  placeholder="От"
                  value={dates.start}
                  onChange={handleDateChange}
                />
                <DateInput
                  name="end"
                  placeholder="До"
                  value={dates.end}
                  onChange={handleDateChange}
                />
                <Button type="submit">Фильтровать</Button>
              </div>
              <Button onClick={handleExcelExport}>
                Экспорт
              </Button>
            </form>
          </div>

          <div className="responses__list">
            {responses && responses.length
              ? responses.map((item) => (
                <div key={item.id} className="responses__item responses-item">
                  <NavLink
                    className="responses-item__link"
                    to={`/forms/${formId}/responses/${item.id}`}
                  >
                    <h2 className="responses-item__title">
                      {`Отклик id: ${item.id}`}
                    </h2>
                    <div className="responses-item__date">
                      {dayjs(item.createdAt).format('DD-MM-YYYY HH:mm')}
                    </div>
                  </NavLink>
                </div>
              ))
              : (<p className="responses__empty">Для данной формы нет откликов</p>)}
          </div>
        </>
      )}
    </div>
  );
}
