import { lazy } from 'react';

export const ResponseList = lazy(() => import('./ResponseList'));
