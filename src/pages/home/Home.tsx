import { PencilIcon, TrashIcon } from '@heroicons/react/24/outline';
import { useEffect, useState } from 'react';
import { NavLink, useLocation } from 'react-router-dom';
import {
  Search, Select, Loader, Modal,
} from 'src/shared/components';
import { formsSlice } from 'src/store/slices';
import type { Form } from 'src/shared/firebase/types';
import './styles.scss';

export default function Home() {
  const { data, isLoading } = formsSlice.useGetUserFormsQuery();
  const [searchForm, setSearchForm] = useState<Form[]>([]);
  const [successModal, setSuccessModal] = useState(false);
  const [deleteForm] = formsSlice.useDeleteFormMutation();

  const formsEmpty = !data?.length;
  const searchEmpty = !searchForm?.length;

  const location = useLocation();
  const from = location.state?.from?.pathname || '/';

  useEffect(() => {
    if (from === '/login' || from === '/signup') {
      setTimeout(() => {
        setSuccessModal(true);
      }, 1000);
    }
  }, []);

  const searching = (name: string): void => {
    if (!data?.length) return;

    if (name.length) {
      setSearchForm(data.filter((item) => item.title.toLowerCase().includes(name.toLowerCase())));
    } else {
      setSearchForm(data);
    }
  };

  const sorting = (val: string) => {
    if (!data?.length) return;

    switch (val) {
      case 'asc':
        setSearchForm([...data]
          .sort((a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()));
        break;
      case 'desc':
        setSearchForm([...data]
          .sort((a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()));
        break;
      case 'resAsc':
        setSearchForm([...data].sort((a, b) => {
          if (a.responseCount && b.responseCount) {
            return a.responseCount - b.responseCount;
          } return 0;
        }));
        break;
      case 'resDesc':
        setSearchForm([...data].sort((a, b) => {
          if (a.responseCount && b.responseCount) {
            return b.responseCount - a.responseCount;
          } return 0;
        }));
        break;
      case 'abc':
        setSearchForm([...data].sort((a, b) => {
          if (a.title.toLowerCase() < b.title.toLowerCase()) {
            return -1;
          }
          if (a.title.toLowerCase() > b.title.toLowerCase()) {
            return 1;
          }
          return 0;
        }));
        break;
      case 'noAbc':
        setSearchForm(data);
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    setSearchForm(data ?? []);
  }, [data]);

  return (
    <div>
      <div className="home-top">
        <div className="home-top__search">
          <Search handlerSearch={searching} />
        </div>
        <div className="home-top__sort">
          <Select
            onChange={(e) => sorting(e.target.value)}
            options={{
              asc: 'Дата создания по возрастанию',
              desc: 'Дата создания по убыванию',
              resAsc: 'Количество откликов по возрастанию',
              resDesc: 'Количество откликов по убыванию',
              abc: 'Название по алфавитному порядку',
              noAbc: 'Не по алфавитному порядку',
            }}
          />
        </div>
      </div>
      {isLoading && <Loader />}
      <div className="home-list">
        {!formsEmpty && !searchEmpty && searchForm.map((item) => (
          <div className="home-item" key={item.id}>
            <NavLink to={`/forms/${item.id}/edit`} className="home-item__edit">
              <PencilIcon width={16} />
              <span className="sr-only">Редактировать форму</span>
            </NavLink>
            <button onClick={() => deleteForm(item.id)} className="home-item__remove">
              <TrashIcon width={16} />
              <span className="sr-only">Удалить форму</span>
            </button>
            <div className="home-item__name">{item.title}</div>
            <NavLink to={`/forms/${item.id}/responses`} className="home-item__responses">
              {`Отклики ${item.responseCount}`}
            </NavLink>
            <NavLink to={`/forms/${item.id}`} className="home-item__survey">
              Пройти опрос
            </NavLink>
          </div>
        ))}
        {!formsEmpty && searchEmpty && (
          <p>Нечего не найдено! Попробуйте ввести другой зарпос</p>
        )}
        {!isLoading && formsEmpty && (
          <p>Нечего не найдено! Создайте свою первую форму</p>
        )}
      </div>
      <Modal
        open={successModal}
        onClose={() => setSuccessModal(false)}
        title="Вы вошли в систему"
      />
    </div>
  );
}
