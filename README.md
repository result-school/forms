# Проект "Формы"

Приложение, позволяющее создавать формы для проведения опросов, получения обратной связи, принятия заявок, регистраций на мероприятия и не только.

## Запуск

`npm install && npm run dev`

## Git Flow

1. Ветки `feature`/`bugfix` создаются от ветки `develop` (пример: `feature/header`)
2. Названия коммитов в соответствии с [Conventional commits](https://www.conventionalcommits.org/en/v1.0.0/)
3. Перед слиянием в `develop` обязательно получить аппрув как минимум от одного участника команды
4. Для релиза ветка `develop` сливается в `main` и ставится [тэг с версией](https://semver.org/)

## Ссылки

- [Notion](https://www.notion.so/vladilen/2-bafa45dc17284239854e4cfd3f7d4c8f)
- [Firebase](https://console.firebase.google.com/u/1/project/groupprojectforms/overview?pli=1)
- [Техническое задание](https://vladilen.notion.site/b4ddbbcccbf941178e095513712aa7f1)
- [Макет](https://www.figma.com/file/uID9MW6eqjpgFCkEAvFxec/%D0%A4%D0%BE%D1%80%D0%BC%D1%8B?type=design&node-id=0-1&mode=design&t=eDr12y1ZgNTl8qMN-0)

## Инструменты

- Vite
- React
- Redux
- Sass
- Tailwind
- Firebase
